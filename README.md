<div align="center">

# Maxim Hohengarten #
*Web developer • graphic designer • 24 years old • France (Haute-Loire)*

</div>

## 📝Projects ##

<div align="center">

### `**🗓️ 2022**` ###

</div>

#### **[Aperigo](https://aperigo.fr/)** ####

- **💭 Concept:** A night delivery service for drinks and snacks (evolution of **Aperibrive** from `2021`)

- **✌️ My part:** Graphic design, logos, flyers, WordPress theme and plugins, deployment

#### **[Glory to the Marquee! (CodePen)](https://codepen.io/Adzetko/full/eYVOGMm)** ####

- **💭 Concept:** Fiddling with a deprecated html tag, the `<marquee>`

- **✌️ My part:** everything!


#### **[Skewed Mosaic (CodePen)](https://codepen.io/Adzetko/full/NWXaxrp)** ####

- **💭 Concept:** Using CSS and the Canvas API for creating a unique visual effect

- **✌️ My part:** everything!


#### **[Multiplex Insoumis](https://multiplex-insoumis.fr)** ####

- **💭 Concept:** chronological streamers list for an event

- **✌️ My part:** All CSS and some components (donation counter and bar)


[comment]: <> (add a demo video/offline of the responsive)

<div align="center">

### `**🗓️ 2021**` ###

</div>

#### **[Escape Ball](https://lesdieuxdudev.lecoledunumerique.fr/jeux/escape-ball/)** ####

- **💭 Concept:** learning how to dev in a team (usage of git), brainstorming and coding a javascript video game (using Matter.js) for a one month deadline

- **✌️ My part:** character design, character physics, input management, sound

- **🖥️ source code:** https://gitlab.com/campus26/simplon1/maxim/brief6


#### **Aperibrive** [(archive)](https://web.archive.org/web/20210305055133/https://aperibrive.fr/) ####

- **💭 Concept:** Having an online shop for a night drink delivery service

- **✌️ My part:** Graphic design, logos, WordPress theme and plugins, deployment


## Skills ##

- 🖥️ Web developer

- 📜 Javascript tinkerer
    - React
    - Vue
    - Web Components
    - Node
    - jQuery (for legacy projects)

- ❤️ CSS enjoyer

- 🎨 Graphic Designer
    - Figma
    - Illustrator / Inkscape
    - Photoshop / GIMP

- 🔓 Open Source enthusiast

- 📝 Writer
